import openai

# connect to local llama.cpp server
client = openai.OpenAI(
    base_url="http://127.0.0.1:8080/v1",
    api_key="sk-no-key-required"
)

# testing api calls
try:
    # Set system prompt
    system_prompt = input("Enter the system prompt: ")

    # Prompt the user for input
    user_prompt = input("Enter a user prompt: ")

    # Insert response code here
    response = client.chat.completions.create(
      model="gpt-3.5-turbo",
      messages=[
        {
          "role": "system",
          "content": system_prompt
        },
        {
          "role": "user",
          "content": user_prompt
        }
      ],
      temperature=0.7,
      top_p=1
    )
    # End Insert response code here

    # return basic clean response
    response_message = response.choices[0].message.content
    print(response_message)

except Exception as e:
    print(f"An error occurred: {str(e)}")
