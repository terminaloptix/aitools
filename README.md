### AI Tools for Local LLM Inference using Llama.cpp with Intel OneAPI Toolkit

#### Arch Linux System Setup (Optional: Use Zen Kernel and Disable Kernel Mitigations)

##### Install Required Packages
```bash
sudo pacman -S git nano wget base-devel cmake openblas intel-compute-runtime ccache gcc
```

##### Install Latest OneAPI Toolkit
```bash
wget https://registrationcenter-download.intel.com/akdlm/IRC_NAS/9a98af19-1c68-46ce-9fdd-e249240c7c42/l_BaseKit_p_2024.2.0.634.sh
sudo sh ./l_BaseKit_p_2024.2.0.634.sh
```

##### Build Llama.cpp with OneAPI
```bash
git clone https://github.com/ggerganov/llama.cpp && cd llama.cpp
source /opt/intel/oneapi/setvars.sh
cmake -B build -DGGML_BLAS=ON -DGGML_BLAS_VENDOR=Intel10_64lp -DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER=icpx -DGGML_NATIVE=ON
cmake --build build --config Release
```

##### Download Models and Scripts
```bash
cd ~/models && wget -i models
wget https://gitlab.com/terminaloptix/aitools/-/raw/main/ai.sh
wget https://gitlab.com/terminaloptix/aitools/-/raw/main/ais.sh
chmod +x ai.sh ais.sh
./ai.sh
```

**Important:** Run `source /opt/intel/oneapi/setvars.sh` manually or add to `.bash_profile` to autoload Intel OneAPI.

#### Open WebUI Frontend Docker Usage (Custom Llama-Server Port)
```bash
docker run -d --network=host -v open-webui:/app/backend/data --name open-webui --restart always ghcr.io/open-webui/open-webui:dev
docker run --rm --volume /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --run-once open-webui
```

#### Open WebUI API Endpoint for Llama-Server
`http://127.0.0.1:8888/v1`

#### Script Notes

* **`ai.sh`**: Prompts to select an LLM model, then executes `./main` from `/build/bin`.
* **`ais.sh`**: Prompts to select an LLM model, then executes a server instance for `./server` from `/build/bin`.
* **`call.py`**: Manually sends an API call to the Llama.cpp server and gets a response.
* **`callprompt.py`**: Prompts the user for a system prompt, then sends an API call to the Llama.cpp server and gets a response.
* **`models`**: A list of current models for use with `wget -i models`.