!/bin/bash

# Get the list of available models
model_dir=~/models/

# Remove vocab files being listed
models=()
for file in "$model_dir"*; do
    filename=$(basename "$file")
    if [[ ! $filename == ggml-vocab-* ]]; then
        models+=("$filename")
    fi
done

# Prompt the user to choose a model
echo "Choose a model:"
select model in "${models[@]}"; do
    break
done

# Run the main script with the chosen model and prompt
~/llama.cpp/build/bin/./llama-server --host 127.0.0.1 --port 8888 -m ~/models/$model -t 4 -c 1024 --top-p 1.0 --temp 0.7 --mlock