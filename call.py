import openai

# connect to local llama.cpp server
client = openai.OpenAI(
    base_url="http://127.0.0.1:8080/v1",
    api_key="sk-no-key-required"
)

# testing api calls
try:
    # Insert response code here
    response = client.chat.completions.create(
      model="gpt-3.5-turbo",
      messages=[
        {
          "role": "system",
          "content": "You will be provided with a description of a mood, and your task is to generate the CSS code for a color that matches it. Write your output in json with a single key called \"css_code\"."
        },
        {
          "role": "user",
          "content": "Blue sky at dusk."
        }
      ],
      temperature=0.7,
      max_tokens=64,
      top_p=1
    )
    # End Insert response code here

    # return basic clean response
    response_message = response.choices[0].message.content
    print(response_message)

except Exception as e:
    print(f"An error occurred: {str(e)}")
